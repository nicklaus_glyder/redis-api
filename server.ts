// Check environment
const env_type = process.env.NODE_ENV || 'dev';

// Get dependencies
import express = require('express');
import path = require('path');
import http = require('http');
import bodyParser = require('body-parser');

// Get our API routes
const app = express();

// Parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Point static path to dist
app.use(express.static(path.join(__dirname, 'dist')));

// Let's me exclude a path from middleware stack
var unless = (path, middleware) => {
  return (req, res, next) => {
    if (path === req.path) { return next(); }
    else                   { return middleware(req, res, next); }
  }
}

// Auth middleware using basic-auth headers
var authenticate = (req, res, next) => {
  let user = auth(req);
  if (user) {
    if (user.name == 'teacher' && user.pass == 't1g3rTester!@#') {
      next();
    } else {
      res.status(401).send('Forbidden');
    }
  } else { res.status(401).send("Forbidden"); }
}

// Raw auth check endpoint
app.get('/auth', (req, res) =>{
  let user = auth(req);
  if (user) {
    if (user.name == 'teacher' && user.pass == 't1g3rTester!@#') {
      res.status(200).send();
    } else {
      res.status(401).send('Forbidden');
    }
  } else { res.status(401).send("Forbidden"); }
})

// Add Auth Middleware to Express resolution, exclude Angular Frontend
import auth = require('basic-auth');
app.use(unless('/angular', authenticate));

// Frontend Angular application
app.get('/angular', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/index.html'));
});

// API Routes, will be protected by auth Middleware
import { redisAPI } from './server/routes/api';
app.use('/', redisAPI);

// Catch all other routes and send 404
app.get('*', (req, res) => {
  res.status(404).send('Not found');
});

// Grab Port from env if present
// Heroku will set port env for security purposes for exmaple
const port = process.env.PORT || '4220';
app.set('port', port);

// Start server and log result + port
const server = http.createServer(app);
server.listen(port, () => console.log(`API running on localhost:${port}`));
