export class Grade {
  id:         number;
  student_id: number;
  type:       string;
  grade:      number;
  constructor({id, student_id, type, grade} : {
               id: number,
               student_id: number,
               type: string,
               grade: number}
             ) {
    this.id =       id;
    this.student_id = student_id;
    this.type =     type;
    this.grade =    grade;
  }
}
