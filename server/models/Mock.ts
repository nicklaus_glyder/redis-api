import { Student } from './Student'
import { Grade } from './Grade'

let Nick = new Student({
  id: 1, grade_ids: [1,2], username: 'nglyder', name: 'Nick Glyder'
});

let Nathan = new Student ({
  id: 2, grade_ids: [3], username: 'nathang', name: 'Nathan Glyder'
});

let Q1 = new Grade ({
  id: 1, student_id: 1, type: 'quiz', grade: 90
});

let Q2 = new Grade ({
  id: 2, student_id: 1, type: 'quiz', grade: 93
});

let T1 = new Grade ({
  id: 3, student_id: 2, type: 'test', grade: 88
});

export const Mock_Students: Array<Student> = [
  Nick, Nathan
];

export const Mock_Grades: Array<Grade> = [
  Q1, Q2, T1
];
