export class Student {
  id:         number;
  grade_ids:  Array<number>;
  username:   string;
  name:       string;
  constructor({id, grade_ids, username, name} : {
               id: number,
               grade_ids?: Array<number>,
               username: string,
               name?: string}) {
    this.id         = id;
    this.grade_ids  = grade_ids ? grade_ids : [];
    this.username   = username;
    this.name       = name ? name : "";
  }
};
