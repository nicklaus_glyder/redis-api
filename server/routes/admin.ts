export const adminRoutes = (client, router) => {
  router.delete('/db', (req, res) => {
    deleteDB(client).then(
      (done) => { res.status(200).send() },
      (err) =>  { res.status(500).send(err); }
    );
  });
};

export const deleteDB = (client) => {
  return client.flushallAsync();
};
