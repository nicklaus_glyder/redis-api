// Router object
import express = require('express');
import { adminRoutes, deleteDB } from './admin';
import { gradeRoutes } from './grades';
import { studentRoutes } from './students';
import { Mock_Students, Mock_Grades } from '../models/Mock';
const router = express.Router();

// Grab Redis client and promisify client functions
import redis = require('redis');
import bluebird = require('bluebird');
       bluebird.promisifyAll(redis.RedisClient.prototype);
       bluebird.promisifyAll(redis.Multi.prototype);
const client = redis.createClient();

client.on('error', (err) => {
    console.log('=== ERROR: ' + err + ' ===');
});

client.on('ready', () => {
  adminRoutes(client, router);
  gradeRoutes(client, router);
  studentRoutes(client, router);
  seedRedis();
})

let seedRedis = () => {
  deleteDB(client).then(
    (done) => { console.log('=== Reset Redis DB Successful ==='); },
    (err) => { console.log('=== ERROR: Could Not Drop DB ==='); }
  );

  Mock_Students.forEach((student) => {
    
  });

  Mock_Grades.forEach((grade) => {

  });
}

export { router as redisAPI };
