import { Grade } from '../models/Grade';
import { Utils } from '../utils/utils';

let redisRoutes = (client, router) => {
  router.get('/grades/:id', (req, res) => {
    client.hgetallAsync(`grade:${req.params.id}`).then(
      (grade) => {
        if ( grade ) { res.status(200).json(grade); }
        else         { res.status(404).send(); }
      },
      (err) => { res.status(500).send(err); }
    );
  });

  router.get('/grades', (req, res) => {
    client.smembersAsync('grades').then(
      (grades) => {
        let allGrades = [];
        for (var grade of grades) { allGrades.push(client.hgetallAsync(`grade:${grade}`)); }
        Promise.all(allGrades).then(
          (values) => {
            // Filter by name
            if(req.query.username) {
              values = values.filter((grade) => {
                return grade.username === req.query.username;
              });
            }
            // Filter by type
            if(req.query.type) {
              values = values.filter((grade) => {
                return grade.type === req.query.type;
              })
            }

            res.status(200).json(values);
          },
          (err) => { res.status(500).send(err); }
        );
      },
      (err) => { res.status(500).send(); }
    );
  });

  router.post('/grades', (req, res) => {
    let fields = ['username', 'type', 'max', 'grade'];
    if (Utils.check_fields(req.body, fields)) {
      client.incrAsync('gradeCount').then(
        (id) => {
          let grade = new Grade( id,
                                 req.body.username, req.body.type,
                                 req.body.max, req.body.grade);
          client.multi()
                .hmset(`grade:${id}`, grade)
                .sadd('grades', id)
                .execAsync().then(
                  (done) => { res.status(200).json(grade); },
                  (err) => { res.status(500).send(err); }
                );
        },
        (err) => { res.status(500).send(err); }
      );
    } else { res.status(400).send(); }
  });

  router.patch('/grades/:id', (req, res) => {
    let fields = ['username', 'type', 'max', 'grade'];
    if (Utils.filter_post(req.body, fields)) {
      client.existsAsync(`grade:${req.params.id}`).then((exists) => {
        if (exists == 1) {
          client.hmsetAsync(`grade:${req.params.id}`, req.body).then(
            (done) => { res.status(200).send(); },
            (err) => { res.status(500).send(err); }
          );
        } else { res.status(404).send(); }
      });
    } else { res.status(400).send(); }
  });

  router.delete('/grades/:id', (req, res) => {
    client.existsAsync(`grade:${req.params.id}`).then((exists) => {
      if (exists == 1) {
        client.multi()
              .del(`grade:${req.params.id}`)
              .srem('grades', req.params.id)
              .execAsync().then(
                (done) => { res.status(200).send(); },
                (err) => { res.status(500).send(err); }
              );
      } else { res.status(404).send(); }
    });
  });
}

export { redisRoutes as gradeRoutes };
