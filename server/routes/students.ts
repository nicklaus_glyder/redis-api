import { Student } from '../models/Student';

let redisRoutes = (client, router) => {
  // Student API
  router.get('/students/:username', (req, res) => {
    client.hgetallAsync(`student:${req.params.username}`).then(
      (student) => {
        if ( student )  { res.status(200).json(student); }
        else            { res.status(404).send(); }
      },
      (err) => { res.status(500).send(err); }
    );
  });

  router.get('/students', (req, res) => {
    client.smembersAsync('students').then(
      (users) => {
        let allStudents = [];
        for (var user of users) { allStudents.push(client.hgetallAsync(`student:${user}`)); }
        Promise.all(allStudents).then(
          (values) => { res.status(200).json(values); },
          (err) => { res.status(500).send(err); }
        );
      },
      (err) => { res.status(500).send(); }
    );
  });

  router.post('/students', (req, res) => {
    if (req.body.username && req.body.name) {
      client.sismemberAsync('students', `${req.body.username}`).then((exists) => {
        if (exists == 0) {
          let user = new Student(req.body.username, req.body.name);
          client.multi()
                .hmset(`student:${user.username}`, user)
                .sadd('students', user.username)
                .execAsync().then(
                  (done) => { res.status(200).json(user); },
                  (err) => { res.status(500).send(err); }
                );
        } else { res.status(400).send(); }
      });
    } else { res.status(400).send(); }
  });

  router.patch('/students/:username', (req, res) => {
    if (req.body.name && !req.body.username) {
      client.existsAsync(`student:${req.params.username}`).then((exists) => {
        if (exists == 1) {
          client.hsetAsync(`student:${req.params.username}`, 'name', req.body.name).then(
            (done) => { res.status(200).send(); },
            (err) => {res.status(500).send(err); }
          );
        } else { res.status(404).send(); }
      });
    } else { res.status(400).send(); }
  });

  router.delete('/students/:username', (req, res) => {
    client.existsAsync(`student:${req.params.username}`).then((exists) => {
      if (exists == 1) {
        client.multi()
              .del(`student:${req.params.username}`)
              .srem('students', req.params.username)
              .execAsync().then(
                (done) => { res.status(200).send(); },
                (err) => { res.status(500).send(err); }
              );
      } else { res.status(404).send(); }
    });
  });
}

export { redisRoutes as studentRoutes };
