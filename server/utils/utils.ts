export const Utils = {
  filter_post: (req, keys) => {
    if (req) {
      Object.keys(req).forEach((key) => {
        if(keys.indexOf(key) == -1) { delete req[key]; }
      });
      return Object.keys(req).length > 0 ? true : false;
    } else { return false; }
  },

  check_fields: (req, keys) => {
    if (req) {
      Object.keys(req).forEach((key) => {
        if(keys.indexOf(key) == -1) { return false; }
      });
      return true;
    } else { return false; }
  }
}
