import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';

import { StudentsResolver } from './students/students.resolver';

const routes: Routes = [
  { path: '', redirectTo: '/home(leftnav:about)', pathMatch: 'prefix' },
  { path: 'home',
    component: HomeComponent,
    resolve: { students: StudentsResolver }},
  // Sidenav routes
  { path: 'about', component: AboutComponent, outlet: 'leftnav'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
