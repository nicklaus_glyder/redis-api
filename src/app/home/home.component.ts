import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { DialogComponent } from '../dialog/dialog.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  btnName: string;

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
    this.btnName = "Click Me!";
  }

  public openDialog() {
    let df = this.dialog.open(DialogComponent);
    df.afterClosed().subscribe(
      result => {
        if(result) {
          this.btnName = result
        }
      }
    );
  }

}
