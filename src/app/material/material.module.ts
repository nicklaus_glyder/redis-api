import { NgModule } from '@angular/core';
import {
        MatToolbarModule, MatSidenavModule,
        MatGridListModule, MatListModule,
        MatCardModule, MatButtonModule,
        MatIconModule, MatSliderModule,
        MatSlideToggleModule, MatTooltipModule,
        MatProgressSpinnerModule, MatProgressBarModule,
        MatCheckboxModule, MatRadioModule,
        MatInputModule, MatSelectModule, MatDialogModule
       } from '@angular/material';

@NgModule({
  imports: [
    MatToolbarModule, MatSidenavModule,
    MatGridListModule, MatListModule,
    MatCardModule, MatButtonModule,
    MatIconModule, MatSliderModule,
    MatSlideToggleModule, MatTooltipModule,
    MatProgressSpinnerModule, MatProgressBarModule,
    MatCheckboxModule, MatRadioModule,
    MatInputModule, MatSelectModule, MatDialogModule
  ],
  exports: [
    MatToolbarModule, MatSidenavModule,
    MatGridListModule, MatListModule,
    MatCardModule, MatButtonModule,
    MatIconModule, MatSliderModule,
    MatSlideToggleModule, MatTooltipModule,
    MatProgressSpinnerModule, MatProgressBarModule,
    MatCheckboxModule, MatRadioModule,
    MatInputModule, MatSelectModule, MatDialogModule
  ],
  declarations: []
})
export class MaterialModule { }
