import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material/material.module';
import { StudentsService } from './students.service';
import { StudentsResolver } from './students.resolver';
import { StudentsListComponent } from './students-list/students-list.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule
  ],
  providers: [
    StudentsService, StudentsResolver
  ],
  declarations: [
    StudentsListComponent
  ],
  exports: [
    StudentsListComponent
  ]
})
export class StudentsModule { }
