import { Injectable } from '@angular/core';
import { Resolve,
         ActivatedRouteSnapshot,
         RouterStateSnapshot } from '@angular/router';
import { Student } from '@models/Student';
import { StudentsService } from './students.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class StudentsResolver implements Resolve<Student> {
  constructor(private backend: StudentsService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) : Observable<any>|Promise<any>|any {
    return this.backend.fetchStudents();
  }
}
