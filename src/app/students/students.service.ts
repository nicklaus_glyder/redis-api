import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { environment } from '@environments/environment';
import { Student } from '@models/Student';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class StudentsService {
  private static _STUDENTS : Student[] = environment.production ? null : [
    new Student('nglyder', 'Nick')
  ];

  constructor(private http: Http) { }

  fetchStudents() : Promise<Student[]> {
    if (StudentsService._STUDENTS) {
      return Promise.resolve(StudentsService._STUDENTS);
    } else {
      return this.http.get('/students').toPromise().then(
        response => {
          if (response.status == 200) {
            StudentsService._STUDENTS = response.json() as Student[];
            return;
          } else {
            return null;
          }
        });
    }
  }

  saveStudent(student: Student) : Promise<Student> {
    // Dummy data short circuit
    if (!environment.production) {
      return Promise.resolve(student);
    }

    return this.http.patch(`student/${student.username}`, student).toPromise().then(
      response => {
        if (response.status == 200)
          return response.json() as Student;
        else
          return null;
      });
  }

  createStudent(student: Student) : Promise<Student> {
    // Dummy data short circuit
    if (!environment.production) {
      StudentsService._STUDENTS.push(student);
      return Promise.resolve(student);
    }

    return this.http.post(`/student`, student).toPromise().then(
      response => {
        if (response.status == 200) {
          StudentsService._STUDENTS.push(response.json() as Student);
          return response.json() as Student;
        } else {
          return null;
        }
      });
  }

  deleteStudent(student: Student) : Promise<boolean> {
    // Dummy data short circuit
    if (!environment.production) {
      StudentsService._STUDENTS.splice(StudentsService._STUDENTS.indexOf(student),1);
      return Promise.resolve(true);
    }

    return this.http.delete(`/student/${student.username}`).toPromise().then(
      response => {
        if (response.status == 200) {
          StudentsService._STUDENTS.splice(StudentsService._STUDENTS.indexOf(student),1);
          return true;
        } else {
          return false;
        }
      });
  }

}
